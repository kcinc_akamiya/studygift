#!/bin/bash

NAME="studygift"                               # Name of the application
DJANGODIR=/www/htdocs/studygift/djangoproject      # Django project directory
# SOCKFILE=/www/hello_django/run/gunicorn.sock     # we will communicte using this unix socket
USER=www-data                                        # the user to run as
GROUP=www-data                                         # the group to run as
NUM_WORKERS=2                                      # how many worker processes should Gunicorn spawn
DJANGO_SETTINGS_MODULE=frespo.settings             # which settings file should Django use
DJANGO_WSGI_MODULE=frespo.wsgi                     # WSGI module name

echo "Starting $NAME as `whoami`"

# Activate the virtual environment
cd $DJANGODIR
source ../bin/activate
export DJANGO_SETTINGS_MODULE=$DJANGO_SETTINGS_MODULE
export PYTHONPATH=$DJANGODIR:$PYTHONPATH

# Start your Django Unicorn
# Programs meant to be run under supervisor should not daemonize themselves (do not use --daemon)
exec ../bin/gunicorn ${DJANGO_WSGI_MODULE}:application \
  --name $NAME \
  --workers $NUM_WORKERS \
  --user=$USER --group=$GROUP \
  --bind=0.0.0.0:8081 \
  --log-level=debug \
  --log-file=/var/log/gunicorn/gunicorn_access_log \
  --error-logfile=/var/log/gunicorn/gunicorn_error_log