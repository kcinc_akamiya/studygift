#! /usr/bin/env python
# -*- mode: python; coding:utf-8; python-indent:4; -*-

from django.contrib.auth.decorators import login_required
from django.http import HttpResponse
from django.shortcuts import render_to_response, redirect
from django.template import RequestContext
from core.models import is_student_user, is_mentor_user, Message, Issue
from itertools import chain
from django.contrib.auth.models import User
from core.decorators import only_post

@login_required
def listSenders(request):
    u"""送受信可能なユーザにリストを表示する"""

    # ログインしているユーザを取得
    user = request.user

    # 表示するタイトルを決定
    title = u""
    if is_student_user(user):
        title = u"メンター一覧"
    elif is_mentor_user(user):
        title = u"学生"
    else:
        # 学生でもメンターでもない場合はメッセージ機能は使えないのでエラーを返す
        return HttpResponse(status=404, content='Sender not found')

    # 送受信可能なユーザを得る
    senders = []
    if is_student_user(user):
        # 学生ならば、自分が作成したイシューを取得
        issues = Issue.objects.filter(createdByUser=user)
        # イシューの確定したメンターを抽出
        senders = []
        for issue in issues.all():
            for mentor in issue.resolved_mentors.all():
                print mentor
        senders = list(chain.from_iterable([issue.resolved_mentors.all() for issue in issues.all()]))
    if is_mentor_user(user):
        # メンターならば、自分がメンタリングしているイシューを取得
        issues = user.resolved_issue_set
        # イシューを作成した学生を取得
        senders = [issue.createdByUser for issue in issues.all()]

    # 情報を行に纏める
    rows = [dict(sender=sender, last_message=Message.get_last_message(user, sender)) for sender in senders]
        
    # コンテキストを設定
    context = {
        'rows': rows,
        'title': title,
    }

    # テンプレートを使って描画
    return render_to_response(
        'core2/list_sender.html',
        context,
        context_instance=RequestContext(request)
    )

@login_required
def listMessages(request, sender_id):
    u"""指定されたsender_idとのメッセージの一覧を表示"""

    # 送信者(受信者)を得る
    try:
        sender = User.objects.get(pk=int(sender_id))
    except:
        return HttpResponse(status=404, content='User not found')

    # ユーザを得る
    user = request.user

    # POSTの場合は..
    if request.method == 'POST':
        # 送信メッセージの内容を得る
        body = request.POST.get(u'body')
        if body:
            # 送信メッセージがあれば登録
            message = Message(body=body, to_user=sender, from_user=user)
            message.save()
    
    # ユーザと送信者の間のメッセージを時間順に得る
    messages = Message.objects.filter(from_user__in=[user, sender], to_user__in=[user, sender]).order_by("create_date")
    
    # 情報を行に纏める
    rows = [dict(message=message) for message in messages]

    # 既読を設定
    for message in messages:
        if message.to_user == user:
            message.readed = True
            message.save()
    
    # コンテキストを設定
    context = {
        'sender': sender,
        'rows': rows,
    }

    # テンプレートを使って描画
    return render_to_response(
        'core2/list_message.html',
        context,
        context_instance=RequestContext(request)
    )


# EOF

