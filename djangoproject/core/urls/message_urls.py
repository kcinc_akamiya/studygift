#! /usr/bin/env python
# -*- mode: python; coding:utf-8; python-indent:4; -*-

from django.conf.urls import patterns, url

urlpatterns = patterns('core.views.message_views',
    url(r'^$', 'listSenders'),
    url(r'^(?P<sender_id>\d+)/$', 'listMessages'),
)

# EOF

