# -*- coding:utf-8; -*-

from core.models import *
from django.contrib import admin


class IssueAdmin(admin.ModelAdmin):
    list_display = ('id', 'createdByUser', 'title', 'description', 'mentor_matching_state')
    search_fields = ['title', 'description']
    list_filter = ('mentor_matching_state',)
    exclude = ('project', 'trackerURL', 'trackerURL_noprotocol', 'is_feedback', 'is_sponsored', 'status', 'logo',
               'total_open_offers_btc', 'total_paid_offers_btc', 'count_solutions_done', 'count_solutions_in_progress')
    


class PaymentAdmin(admin.ModelAdmin):
    list_display = ('id', 'creationDate', 'status', 'total', 'currency', 'offer_currency', 'usd2payment_rate')


class UserInfoAdmin(admin.ModelAdmin):
    list_display = ('id', 'user', 'user_type', 'realName', 'website', 'prefecture', 'other_contact',
                    'academic_background', 'society_experience', 'nearest_station')
    list_filter = ('user_type',)
    
admin.site.register(UserInfo, UserInfoAdmin)


# 今回のシステムではダミープロジェクトを用意して、全てそのダミープロジェクトに対するイシューを
# プロジェクトのように取扱ので、管理画面側にプロジェクトを表示しない。この為、コメントアウト。
#admin.site.register(Project)

admin.site.register(Issue, IssueAdmin)
admin.site.register(IssueComment)
admin.site.register(Offer)
admin.site.register(OfferComment)

# 今回のシステムでは、ソリューションは使用しないと思われるので、コメントアウト。
#admin.site.register(Solution)

admin.site.register(Payment, PaymentAdmin)

class MessageAdmin(admin.ModelAdmin):
    list_display = ('id', 'from_user', 'to_user', 'body', 'create_date', 'readed')

admin.site.register(Message, MessageAdmin)


class EvaluationAdmin(admin.ModelAdmin):
    list_display = ('id', 'issue', 'mentor', 'evaluated', 'motivation_rank', 'action_rank', 'necessity_rank')

admin.site.register(Evaluation, EvaluationAdmin)

# EOF
