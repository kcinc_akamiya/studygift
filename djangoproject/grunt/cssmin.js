module.exports = function(grunt, options) {
	var stfile = 'staticfiles/static/css2/';
	var files = {};
	files[stfile +'/studygift.min.css'] = stfile + 'studygift.css';
	return {
		build: {
			files: files
		}
	};
};