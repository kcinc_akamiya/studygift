# -*- coding: utf-8 -*-
__author__ = 'kurata'

from django.forms import *
from django.utils.translation import ugettext_lazy as _
from localflavor.jp import forms as jp_forms
from registration.forms import RegistrationFormUniqueEmail
from frespo.settings import USER_STUDENT, USER_MENTOR, USER_SUPPORTER

# CharField.widget_attrs()

class RegistrationForm(RegistrationFormUniqueEmail):
    u"""登録フォームの共通部分"""

    def __init__(self, *args, **kwargs):
        u"""コンストラクタ"""
        super(RegistrationForm, self).__init__(*args, **kwargs)

        # 上位から継承するフィールの設定をカスタマイズ
        
        # ユーザー名(ユーザーID)の設定
        self.fields['username'].widget.attrs.update({'class': 'form-control'})
        self.fields['username'].initial = ''
        
        # メールアドレスの設定
        self.fields['email'].widget.attrs.update({'class': 'form-control'})
        self.fields['email'].initial = ''

        # パスワードの設定
        self.fields['password1'].widget.attrs.update({'class': 'form-control'})
        self.fields['password1'].initial = ''

        # 確認用パスワードの設定
        self.fields['password2'].widget.attrs.update({'class': 'form-control'})
        self.fields['password2'].initial = ''

    # フィールドの定義
    #
    # 継承される子クラスで必要になる全てのフィールドをここで定義。
    # どの子クラスも同じテーブルにアクセスするので、一括して共通の親クラスにてフィールドを定義しておく。
    # 各フォームはテンプレート側で必要なフィールドを使う事。

    # 氏名
    realName = CharField(required=True,
                         widget=TextInput(attrs={
                             'class': 'form-control'
                         }),
                         initial=""
    )
    
    # 予備の連絡先
    other_contact = CharField(required=True,
                              widget=TextInput(attrs={
                                  'class': 'form-control'
                              }),
                              initial=""
    )
                           
    # SNSのURL?
    website = URLField(required=True,
                       widget=TextInput(attrs={
                           'class': 'form-control'
                       }),
                       initial=""
    )

    # 自宅からの最寄り駅
    nearest_station = CharField(required=False,
                                widget=TextInput(attrs={
                                    'class': 'form-control'
                                }),
                                initial=""
    )

    # 最終学歴
    academic_background = CharField(required=False,
                                    widget=TextInput(attrs={
                                        'class': 'form-control'
                                    }),
                                    initial=""
    )

    # 社会人の経験年数
    year_of_career = CharField(required=False,
                               widget=TextInput(attrs={
                                   'class': 'form-control'
                               }),
                               initial=""
    )
                           
    # 職務経歴
    job_career = CharField(required=False,
                           widget=Textarea(attrs={
                               'class': 'form-control'
                           }),
                           initial=""
    )

# 学生・メンター用の移住地の選択肢
PREFECTURE_CHOICES_FOR_STUDENT_OR_MENTOR = (
    (u"東京", (
        (u"（23区内）", u"（23区内）"),
        (u"（都下）", u"（都下）"),
    )),
    (u"埼玉", (
        (u"（東部）東北本線・高崎線沿線とその周辺", u"（東部）東北本線・高崎線沿線とその周辺"),
        (u"（西部）東武東上線・八高線沿線とその周辺", u"（西部）東武東上線・八高線沿線とその周辺"),
    )),
    (u"千葉", (
        (u"（中部・南部）総武線沿線および以南", u"（中部・南部）総武線沿線および以南"),
        (u"（北部）常磐線・北総線沿線とその周辺", u"（北部）常磐線・北総線沿線とその周辺"),
    )),
    (u"神奈川", (
        (u"（東部）横浜・川崎・鎌倉市内と横須賀線沿線", u"（東部）横浜・川崎・鎌倉市内と横須賀線沿線"),
        (u"（中部・西部）相模線沿線および以西", u"（中部・西部）相模線沿線および以西"),
    )),
)

    
class StudentRegistrationForm(RegistrationForm):
    super(RegistrationForm)

    # ユーザータイプ
    user_type = IntegerField(widget=HiddenInput(), initial=USER_STUDENT)

    # 居住地域
    prefecture = CharField(required=True,
                           widget=Select(choices=PREFECTURE_CHOICES_FOR_STUDENT_OR_MENTOR),
                           initial=""
    )

                             
class MentorRegigstrationForm(RegistrationForm):
    super(RegistrationForm)

    # ユーザータイプ
    user_type = IntegerField(widget=HiddenInput(), initial=USER_MENTOR)
    
    # 画像
    profile_image = FileField(required=True,
                              widget=FileInput(attrs={
                                  'class': 'form-control'
                              }),
                              initial=""
    )
    
    # 居住地域
    prefecture = CharField(required=True,
                           widget=Select(choices=PREFECTURE_CHOICES_FOR_STUDENT_OR_MENTOR),
                           initial=""
    )


class SupporterRegistrationForm(RegistrationForm):
    super(RegistrationForm)

    # ユーザータイプ
    user_type = IntegerField(widget=HiddenInput(), initial=USER_SUPPORTER)

    # 居住地域
    prefecture = CharField(required=True,
                           widget=jp_forms.JPPrefectureSelect(attrs={
                               'style': 'display: block',
                               'class': 'form-control'
                           }),
                           initial=""
    )

# EOF
