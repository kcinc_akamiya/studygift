# -*- coding: utf-8 -*-

from django.shortcuts import render
from registration.backends.default.views import RegistrationView
from auth.forms import *

class StudentRegistrationView(RegistrationView):
    form_class = StudentRegistrationForm
    template_name = 'registration/registration_student_form.html'
    http_method_names = ['get', 'post']
    pass


class MentorRegistrationView(RegistrationView):
    form_class = MentorRegigstrationForm
    template_name = 'registration/registration_mentor_form.html'
    http_method_names = ['post', 'get']


class SupporterRegistrationView(RegistrationView):
    form_class = SupporterRegistrationForm
    template_name = 'registration/registration_supporter_form.html'
    http_method_names = ['post', 'get']
    pass
