# -*- coding: utf-8 -*-
__author__ = 'kurata'

from django.shortcuts import redirect
from django.conf.urls import url, patterns
from django.views.generic import RedirectView, TemplateView
from auth.views import *
from auth.forms import *

urlpatterns = patterns(
    url(r'^$', SupporterRegistrationView.as_view(form_class=SupporterRegistrationForm), name='register_default'),
    url(r'^student/$', StudentRegistrationView.as_view(form_class=StudentRegistrationForm), name='registration_student'),
    url(r'^mentor/$', MentorRegistrationView.as_view(form_class=MentorRegigstrationForm), name='registration_mentor'),
    url(r'^supporter/$', SupporterRegistrationView.as_view(form_class=SupporterRegistrationForm), name='registration_supporter'),
)
